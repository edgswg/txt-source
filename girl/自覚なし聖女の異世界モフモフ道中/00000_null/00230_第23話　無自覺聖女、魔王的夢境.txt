「……這是夢吧。」
薩格基爾獨自一人坐在冰冷的鋼之玉座上。

目前的自己並沒有足以翹起的長腿，也沒有能夠撐住臉頰的手臂。

有著短小手足的醜陋毛皮才是自己目前的姿態。

薩格基爾依然記得這冰冷空氣。

以及那令人懷念的魔王城香味。

自己過去曾支配過暗黑大陸。

那是令人懷念的和平時代。

所以，這是場夢。

這時期大概是統一暗黑大陸後過了數百年的時候吧。

而且，這裡若是重現過去的夢境，那麼接下來的展開薩格基爾也清楚。

『魔王薩格基爾啊，為何不往人類所居住的大陸侵略。統一暗黑大陸的歷代魔王，全都會以人類大陸為目標，伸出侵略之手才對。』
沒有身影的人朝薩格基爾說道。

那是清廉響亮的女性聲音。

薩格基爾知道這聲音的真面目。

正因為知道，才會對此蔑視失笑。

「為什麼不侵略人類界，嗎？庫庫，那余反問你，為何作為人類友方的你會在意這種事？余若是不威脅人間界，保持和平不是好嗎。」
『……』
不知是否被戳中痛處，女神保持沉默。

「余之所以統一暗黑大陸，也只是討厭這滿是爭鬥的大陸。這塊陸地已經有足夠的資源，事到如今已經不需要余去搶奪土地，獲取大陸和平的現在，正好適合緩緩開發土地。根本沒必要和人類之間引起戰爭。」
『……你這樣還能算是殘虐魔族的王，魔王嗎？』
「正因為是魔【王】。身為王，才需要經常思考國民的事。余是魔族，毫無爭鬥的生活或許稍嫌無趣，但這已經在統一戰爭中盡情揮霍過。從今以後必須回復國力、治癒人民。余打算在此構築千年王國。」
這是任何魔族都無法完成的偉業。

『你是認真的？成為魔王的現在，你應該會被對人類無可奈何的憎恨所驅動才對。為什麼能夠忍耐……！？』
「露出馬腳了啊，女神。余之所以能夠忍耐是因為理解了構成。」
沒錯，只要明白盤旋於腦海中的黑色期望並非自身所期望，就能將其趕至意識角落。

「歷代魔王的所有人全都毫無理由的虐殺人類，並在虐殺過後被為了與其對抗而誕生的勇者所討伐。好幾年好幾年，全都是你們從中作梗。」
『你到底知道到什麼程度……？』
「余什麼都不知道。余只知道我等魔族只是為了將靈魂送往上位存在的收穫機，而人類則是你們的糧食。只要增加就收穫，減少只需要培育即可。」
薩格基爾從玉座上站起，瞪向高高的天花板，以及更上方的上位存在。

「余可不打算任由你們所製作的狗屎構造給吞噬。余只會以余的想法來支配世界。可別小看余了啊，裝做女神的邪神。」
吱，如同摩擦般嘴角揚起，薩格基爾嘲笑著。

『……是嗎，我明白了。我判斷你不能使用，得重新選擇魔王了呢。』
「不打算隱藏本性嗎，只會授予神託的沒用傢伙，又能對余做什麼？」
雖然嘲笑了對方，但對手可是上位存在，屬於高位次元的怪物。

薩格基爾雙手握拳，為了戰鬥而精煉魔力。

女神以哀傷的口氣朝薩格基爾說。

『魔王薩格基爾，毫無慈悲的冷酷男子啊……。就給你個試練吧……』
「……試練？你在說什麼？』
『沉溺於力量、姿意妄為，並虐待人民的惡行，身為女神可不能置之不理。』
對於說出與剛才的對話完全相反的女神，薩格基爾驚訝地挑眉，正想要回答時卻突然嚇了一跳。

「你這傢伙……！打算強行將余納入神之試練的定義內嗎……！」
『這是沉重的試練。你將失去所有力量，趴在地上過活吧。然而，神是寬容的。是不會給予你無法越過的試練。』
「咕、咕奴奴奴──！身體、身體像是被燒過一樣，好熱……！」
身體冒出煙，所能看見的視野跟著降低。

手腳變短，全身覆蓋著黑毛，就連聲音都無法正常發出。

『給予你的考驗只有一個。那就是以此身接受百萬的愛。不受任何人愛戴的冷酷之王啊，請打從心底理解被人喜愛的感覺吧。』

從天上降下神聖光芒，核桃般大小的種子落下。

『只要種子發芽、開花，你的詛咒就能解除，恢復原本的模樣吧。但是，在那之前你的模樣都只是隻醜陋的小動物。要以那副模樣獲得百萬的愛或許會很辛苦吧，不過，這沒什麼，你一定能夠跨越。我會期待你跨越試練，成為溫柔魔王的那天到來。』
「你這傢伙……！這算什麼試練啊……！這不就是個卑劣的詛咒嗎……！你這邪神……你這該死的邪神啊……！」
已經只能發出咩咩的身體，就這麼從玉座上滾了下來。

在薩格基爾緩緩起身前，掉落的種子已經滾了下去。

「還想著狀況很奇怪才過來看看，沒想到事情會變成這樣啊……』
有人比薩格基爾更早撿起種子。

『喔、喔，是札柏克嗎……！』
被認定為心腹的男子俯視薩格基爾。

消瘦的肌膚和雙頰，只有落下的目光閃閃發光。

「嗯……」
札柏克仔細觀察手中的種子。

「這不是詛咒的原因，而是用來檢測詛咒是否解除的咒物吧。就算破壞了也沒有意義。」
『這樣子啊。但是，身為余右手的貴公能理解事情的狀況真是得救了。居然說是百萬的愛啊。那女神，是沒能看見余多麼受到余國民的支持嗎。若是得到身為宰相的貴公保證，國民也能理解這副模樣的余就是魔王薩格基爾吧。像這種詛咒馬上就可以解開了。』
札柏克挑起一邊眉毛。

「最後？真不可思議，我的眼前都沒有出現魔王大人呢。」
『札柏克？你在說什麼，不是聽見余和女神的對話嗎？余是魔王薩格基爾啊。』
「你只是一隻咩咩叫的吵人野獸。我可不記得自己認識這種傢伙。』
『什……！？札柏克，你這傢伙難道打算背叛余！？』
「衛兵！魔王大人不見了啊，你們在幹什麼啊！」
無視痛斥自己的薩格基爾，札柏克高聲說道。

身穿鎧甲的士兵推開厚重的鐵門進入其中。

「這、這究竟是……！？札柏克大人，魔王大人是去哪裡了！？」
「調查這些是你們的工作吧！怠慢護衛的任務，可是會受到重罰的！」
被喝斥的衛兵們縮了縮身體。

「不過，現在先去找出魔王大人才是最重要的。對國內發出告示，一定要找到魔王大人！」
「「「是！」」」
看著毫無懷疑朝自己敬禮的衛兵，札柏克笑了。

「……啊，另外還有隻害獸進來了。把這傢伙帶到城外處理掉。」
札柏克緊緊抓住薩格基爾的脖子，朝衛兵伸出。

『等、等等。余可是魔王─薩！？』
全身被麻痺，薩格基爾就此失去意識。

「或許有奇怪的疾病，可要小心屏住呼吸啊。」
札柏克將吃下一記電擊魔法而冒出煙的毛球強推給衛兵。

衛兵訝異地接過毛球，行過一禮後離開玉座之間。

札柏克在目送他們離開後，便坐在玉座上用手撐起臉頰。

「庫庫庫，多麼幸運啊。曾經放棄過的地位，居然這麼輕易就到手了。」
札柏克本來就想背叛魔王。

但是，就在他準備放棄對毫無破綻的薩格基爾動手時，這次的騷動發生了。

因此他對女神滿是感謝。

「這麼說來，那女神好像說了得重新選擇魔王啊。雖然不清楚神那邊的狀況，不過只要能夠讓世界重回戰亂就行了。掠奪和虐殺，這才是魔族的本性。就讓我們好好享受一下吧！」
滾動手中的種子，札柏克持續高聲笑道。


†††


自此之後就是地獄了。

忠實的衛兵在刺傷了失去意識的薩格基爾後，點了把火把牠和垃圾一起丟掉。然而，薩格基爾依然保住了一命。

受到詛咒的身體雖然脆弱，但或許是詛咒的關係，牠並沒有死掉。

被趕出城堡的薩格基爾，為了讓人相信牠是魔王而與各種人搭話，然而誰也不相信他所說，殘酷地對待牠。

被人踩踏、嘲笑、戲弄傷害，薩格基爾持續了數百年這樣的生活。

喝泥水、吃蟲和草充飢。

太悲慘，太痛苦了。

對女神的憎恨、對背叛的部下的憤怒，在這數百年間都被磨光了。

不知道自己為何而生，只是流浪的每一天。

放棄恢復原狀的某天，薩格基爾掉進海裡。

接著便隨著海流漂流至人類的大陸，然而，在哪裡也沒有任何變化。

畢竟這只是個連史萊姆都贏不了的貧弱身體。

只是改變對象，被虐待的生活依舊沒有任何改變。

但是，這天卻有些許不同。

『咕嘎嘎嘎嘎嘎！真難看啊，魔王大人！』
『這傢伙真的是原魔王嗎！？未免也太弱了吧！』
在空中飛翔的兩隻巨鳥發出刺耳的鳴叫聲嘲笑。

『你、你們這些傢伙......』
薩格基爾搖搖晃晃地站起身，瞪向空中。

但是，從口中所出的卻是如同幼貓般『咩咩』的軟弱叫聲。

『話說回來，這也太頑強了吧。就算我們只是在玩，也差不多該死掉了喔？不然的話，根本沒法完成命令。』
『就算腐爛了，依然是原魔王呢。沒有牙齒和爪子，只是隻軟綿綿的毛球，可是完全沒有要死掉的跡象呢。』
如同那些傢伙所說，要是受到如此執著的攻擊，一般來說早就會死了。

但是，這可不行。

這具肉體的體質看似差勁，可是就算想死也死不了。

如同外表所見只是具毫無力量的垃圾身體，生命力卻是無限。

這也是神的詛咒之一吧。

無論受到多少程度的傷、遭到毒害、溺水、被點燃、飢餓，都不會死掉。

即便如此，只有痛苦確實會感受到，所以相當過分。

確實是過著水深火熱的每一天，但是今天襲擊自己的敵人卻與平時的魔物不同。

就像是知道自己的真面目，被人下令而行動。

事到如今，就算殺了失去力量的自己到底還有什麼用。

要是能夠殺了自己，乾脆就在這裡殺了吧。

已經累了，不想在獨自一人悲慘的生活下去。

就在這時。

「到此為止！」
某人的身影介入自己與巨鳥之間。

就像是要守護自己般而擋住巨鳥們的人影，是個漂亮的少女。

「我可不容許你們再對這毛茸茸的小傢伙動粗了！」
毅然地說出口，轉眼間便將襲來的巨鳥們打倒。

接著溫柔治癒受傷後而驚呆的自己。

時隔數百年，不，或許還是打從出生以來第一次被人這麼溫柔對待。

被人抱起，溫柔撫摸。

他人的溫暖，原來是這麼舒服嗎。

抬起頭，便與那大大的，宛若黑曜石般的目光相對。

「嗯，怎麼啦？」
少女莞爾一笑。

『余……』
看著少女的微笑，薩格基爾便覺得自己的胸口被堵住，接著便感覺到溫暖。

被詛咒後過了數百年，薩格基爾還是第一次接觸到愛。