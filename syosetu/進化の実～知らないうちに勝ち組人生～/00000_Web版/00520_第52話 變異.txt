﻿
自露易絲出發已經過了一周了。
但是，怎麼說呢，在這一周什麼都沒有發生，我依舊在皇宮裡進行魔法訓練。
順便一提，今天莎莉婭她們各自去接受委託所以不在場，奧爾嘉醬也和莎莉婭在一起行動。
然而現在，我終於到達了這個境界。

「！」
「哦！」

突然，在我精神力高度集中下，成功使出了。
有著籃球大小的初級水魔法『水球』

「成功了，誠一！完全沒有不穩定的跡象......這個，應該是得到技能『手下留情』了呢？」

我的指導，弗羅利歐對我這麼說，而我自己也十分確信肯定是得到了『手下留情』的技能。
因為這個大小不會造成破壞！
我喜悅的流下眼淚，在心中讚揚著這一個月來持續努力的我自己。
果然，腦內一直出現的聲音響起了。

『掌握了技能『無間地獄（無限地獄）』。』

發生了什麼？
稍等一下，冷靜下來，冷靜下來，不要緊的！
一定是我的幻覺！
重新深呼吸一下，剛才腦中的聲音又響起了。

『技能『無間地獄』──

「我勒個去！」
「！？」

我為了確認自己是否清醒，讓自己的臉和地面進行了♂親♂密♂的接觸。
對我奇特的行為吃了一驚的弗羅利歐問我。

「怎，怎麼了，誠一......。」
「不，什麼事都沒有喲？只是聽不到獲得技能『手下留情』的訊息，所以直接拿身體試一下，真的，什麼，奇怪的事也沒有。」
「嗯，沒事就好！」

弗羅利歐露出了苦笑的表情。
已經不能忍了！
什麼！？

『無間地獄！？』技能
『手下留情』去哪裡了？
為什麼『手下留情』會變成地獄那樣的危險情況。
不，稍微冷靜一下！
現在丟棄希望不是還有點太早了嗎？
你看，也有雖然名字看起來危險，但內容並不是那麼可怕的東西的可能性吧！
我抱著一點點的希望，確認了技能效果。

『無間地獄』――給予對象無限痛苦的技能，技能發動時，無論怎麼攻擊對象，對象都不會死，這樣給與對象死一般的痛苦。普通的手下留情也行，隨意發動。

和名字一樣恐怖呀呀呀呀呀呀呀呀呀呀呀呀呀！！！
好恐怖！？
這是什麼技能！
這不是像我這種有著怪物數值的人絕對不能得到的技能嗎！？
話說最後一條是什麼，普通的手下留情也可以？
完全成為了贈品不是嗎！？
......怎麼辦，和我追求的成長方向完全不一樣。
我的明天在哪裡......（死掉的眼神）
然後，看到了不是這個王城裡的人走了進來。

「咦？伽魯斯先生？」
「唔？哦，這不是誠一君嘛！」
「哎呀？是真的啊！」

在王城裡看見的是公會會長伽魯斯和接待員愛麗絲小姐。
愛麗絲小姐雖然是接待員的打扮，但是伽魯斯依舊是三角短褲，展現著自己的裸體。
沒常識也要有個限度吧！
話說，門衛先生你也攔截一下這可疑人物呀！

「誠一為什麼在這裡？」
「我是在王都杯上得到冠軍，獲得了和露易絲共同訓練的權利。」
「......你還真的是在我不知道的地方做著各種各樣的事啊。」

對於我的話，伽魯斯先生露出了苦笑。

「倒是伽魯斯先生，你問為什麼會來王城？」
「我們是為了把一個人帶到殿下身邊才來的。」
「是哪位？」

不知道指的是誰，我歪著頭看去，發現伽魯斯他們後面，有一位老人。

「呵呵，出次見面，年輕人。」
「嗯，啊......總覺得......。」

這個老人給我的第一印象是，仙人。
白色的長眉毛，白色的長髮，還有非常長的白鬍子。
身披白色長袍，身高不算太高，但是帶著和我差不多高的巨大拐杖。
有著溫柔的眼神，給人一種安心感的氣氛的老人。

「我是巴拿帕斯‧艾布里德，隨便叫我巴拿就好。」
「啊，我是誠一。」

真是爽快的打招呼呐，巴拿先生。
我也說了自己的名字，不過弗羅利歐先生，一臉感動至極的樣子。

「啊，我竟然能和那個『魔聖』巴拿帕斯‧艾布里德見面！」
「唉？弗羅利歐先生，你知道巴拿帕斯先生麼？」
「那是當然的吧！是那個巴拿帕斯‧艾布里德大人喲！毫不誇張地說，他可是世界第一魔法師喲！」
「唉，是嗎？......。」

對於特別興奮的弗羅利歐先生，我被那氣勢逼的向後退。
弗羅利歐先生都那麼說了，所以就是很厲害的人吧！

「沒有那麼誇張，呵呵，那麼讚揚我也沒什麼好處喲，『冰麗的魔人』先生！」
「您知道我的事嗎！？」
「嗯，經常從那裡聽到你這位優秀的魔法師。」
「！？」

巴拿帕斯的一句話，讓弗羅利歐哭了出來。
這就像是和有名的偶像說話，謎一般的狂熱心境吧。
總之能看到弗羅利歐先生意外的一面，真是很有意思。

「而且，老身也還沒達到魔道的極點呢。正因為老身是精靈，才能活那麼長的時間，儘管如此也還在追逐魔道的極點呢。」
「唉，巴拿帕斯先生，您是精靈嗎？」
「看不出來嗎？你看我的耳朵。」

一邊說著，一邊把耳朵露了出來。
就像巴拿帕斯先生所說的那樣，是和普通人的有所不同，前端尖尖的耳朵。
話說，首次在異世界遇到精靈，而且還是名人。
偷偷地為這件事所感動，然後從王城裡，有人向我們這邊走來。

「哦！誠一，訓練的情況怎麼樣了？」
「啊，藍澤先生！」
「啊，怎麼了，為什麼伽魯斯他們在這裡？」

從城裡出來的，是這個國家的王，藍澤先生。
然後，除了我和弗羅利歐以外，伽魯斯他們看到藍澤先生都露出一副驚訝的表情。

「過得還好嗎，藍澤？」
「啊？......啊！老師！？」

藍澤先生注意到巴拿先生後，露出了十分吃驚的神情，立刻端正了姿勢。

「久疏問候了！老師也還好吧......。」
「不用那麼緊張也行吧？你現在可是一國之主了，不用對我這個像鷹一樣的老糊塗蟲那麼客氣。」
「沒，沒有那樣的事！」

巴拿先生，到底是什麼人。
那個，我是不是也轉換一下語氣，好像就我一個人在進行普通的對話。
身為國王的藍澤先生，也是十分吃驚的樣子，會這麼想也沒辦法吧。

「嗯......藍澤先生，你和巴拿帕斯先生是什麼關係呢？」
「哎？啊啊，就像我說的那樣，巴拿帕斯先生是我的恩師喲！」

原來如此，所以藍澤先生在巴拿先生面前抬不起頭。
這樣就對巴拿先生的身份有所瞭解了，然後伽魯斯先生更進一步的給我提供資訊。

「不僅如此巴拿先生還在擔任『巴帕德爾魔法學園』的學員長。」
「『巴帕德爾魔法學園』！？」

我對這個名字感到吃驚，
因為那學校......
是翔太他們所在的學校。
而表現出吃驚的並不只有我一個，藍澤先生也是一臉不可思議的表情向巴拿先生詢問。

「那麼，老師，您為什麼會在這裡？特別會面的預訂應該是沒有的。」
「嗯，其實是有事想告訴你。」
「有事想傳達？」
「是您兒子的事！」
「！？」

對於巴拿先生的話，藍澤先生的表情發生了變化。
......這麼說來，在奧爾嘉醬襲擊藍澤先生的時候。
也聽說過第一王子，第二王子，以及第一王女全部都在學園裡面。
回想起了以前聽過的話，巴拿先生繼續說。

「現在，老朽的學院裡情況有點特殊，你知道原因嗎？」
「......是因為勇者的存在嗎？」
「差不多是那個原因吧，所以老朽的學園內發生了一些麻煩事。」
「麻煩的事？」
「那是――。」

在巴拿先生說出重要情報的那一瞬間，有一名士兵抱著決死的表情跑了過來。

「陛下！大事不好了！」
「怎麼了？那麼慌張？」

藍澤先生向士兵詢問。

「魔物！魔物的大軍正在向這裡進攻！」
「！？」

聽到這個資訊在場的所有人臉色都變了。
不過，露易絲應該是去國境附近調查魔物活躍的原因而出發了......

「這是怎麼回事，魔物在國境附近活躍起來......而且也為了討伐派出了『黑之聖騎士』和露易絲。」

藍澤先生也和我抱有同樣的想法，但是士兵繼續說。

「剛才和露易絲隊長聯繫，得到了她那邊也有魔物侵略的事實。『黑之聖騎士』那邊也說了同樣的話。」
「怎麼會這樣，大量魔物同時開始活躍......。」

對於士兵的話，藍澤先生陷入了沉思，但是巴拿先生訓斥道。

「藍澤，那件事情之後再說，眼前的情況比較重要吧？」
「嗯......沒錯呢，老師。喂，能夠知道魔物的數量嗎？」
「是！根據偵察部隊，大約有五千左右。」
「五千......怎麼可能！？」

對於士兵提供的資訊，藍澤先生咂了咂舌，立刻下令。

「喂，現在能立刻召集的部隊能有多少？」
「那個......其他的部隊，大部分在其他區域遠征中，目前能立刻召集的只有皇宮中的士兵。」
「是嗎......？好，你現在就去召集皇宮裡的士兵，向偵察部隊傳遞返回的指示，趕緊去吧！」
「遵命！」
「弗羅利歐！」
「是！」
「等偵察部隊回來時，通過偵察部隊的資訊放出飛行式魔導照相機。
命令能維持影像的最低人數即可，其餘的人和偵察部隊會合後一同進行對魔物的討伐。」
「遵命！」

弗羅利歐先生聽到藍澤的指示後，馬上開始移動。
面對開始變得慌亂的皇宮，伽魯斯他們也開始了行動。

「我們公會的力量也是必要的吧？」
「抱歉，變成這樣的事態，能夠配合吧？」
「沒有問題！面對這樣的事態，我們沒有撒手不管的道理。」

一邊這麼說著，一邊展露出肌肉的伽魯斯，對愛麗絲小姐作出指示。

「愛麗絲！你加速趕往公會，發佈魔物討伐任務！」
「瞭解！」
「十分感謝，關於報酬方面國家會好好支付的。」
「哈哈哈，那是，其他的冒險者也是鼓足幹勁呢！我也一樣，可以好好大鬧一場的機會怎麼能錯過！」
「這次請讓我去前線戰鬥！」
「誒！？兩個人都要去嗎？」

不是吧，愛麗絲小姐甩了下鞭子，不過那是SM技術吧，和戰鬥有關係嗎？
嘛，這個場面的話伽魯斯先生的肌肉倒是很適合。

「真是失禮呀！，正是在這種時候，我的肌肉就像能噴出火一般炙熱！而且，這一次也能好好地把我的肌肉展示在大眾面前！」
「最後那個才是真心話吧！」

在這裡，伽魯斯的肌肉並不是很有名呐。

「我在成為接待員之前也是很有名氣的喲。」
「那還真是無法想像。」

有名氣......所以表現才那麼奇怪嗎？
無視露出微妙表情的我，伽魯斯先生他們回公會了。
然後這裡只剩下藍澤先生，巴拿先生和我了。

「放心，呵呵呵，這次我也把力量借給你吧。」
「誒！老師也要參加戰鬥嗎？」
「學生遇到了危機，這是理所當然的吧！」
「啊，謝謝！」

藍澤先生低下了頭。

「身為『超越者』的老師借給我力量什麼的......真是對於現狀真是運勢好呢！」
「『超越者』？」

對於不熟悉的單詞感到疑惑，藍澤先生幫我解釋。

「不知道嗎？『超越者』就是指那些超越人類界限的人，露易絲是人類的最高頂點，500級，但是還有些人，能罕見的超越這個界限變得更強。」
「那個天才呀？如果是那個小姑娘的話，和我一樣成為超越這只是時間問題。」

難道......
突破人類等級上限，就可說是超越人類那樣的東西。
......咦，為什麼呢，我似乎也快要成為『超越者』的氣氛？
啊，沒問題，以為我現在只有十五級而已。
雖然十五級打倒五百級的事情很奇詭，但是不用介意，不用介意......

「然後呢？誠一，你怎麼辦？」
「哎！？」
「就像伽魯斯說的那樣，這次的事態很緊急，但對於冒險者來說，是以委託的形式參加的，所以並沒有強制......。」
「我也會戰鬥哦，因為，我喜歡這個城市。」

對於我坦率的言語，藍澤先生一瞬間睜大了眼睛，然後高興的笑了。

「是嗎？......那麼，把力量借給我吧！」
「瞭解！」

就這樣，我下定了與蜂擁而至向這街道湧來的魔物戰鬥的決心（虐殺他們的決心）。

◇◇◇◆◆◆◇◇◇

――時間稍微回溯
在一個被黑暗支配的房間裡，一個男人面對著水晶發出了奇妙的笑聲。

「呵呵呵呵呵呵！做到了！這樣的話......離復活魔神大人又近了一步！」

這是從水晶的另一邊傳來了男人的聲音。

『哼，做準備的可是這邊呢？究竟是誰在『山』附近設置了轉移魔法呢？』
「認真的表示感謝，不過我這邊也為了收集魔物很累呀，就結果來看付出的辛苦相同吧！」
『......哼......』

男人不高興的聲音從鼻子透過魔法水晶傳了過來。

「算了，從這裡看有很多的士兵在向『山』這邊聚集。魔物有多少送來多少，因為雖然這裡的『山』有不少，但能傳送的只有這裡吧。」
『嘛，說的也是，要是海裡能設置魔法陣就好了，不過......』
「沒辦法！設置轉移的魔法陣必須是在陸地上才能發揮效果，雖然這麽說，不過沒有聽說過這裡有能使用空間魔法的人，托這的福，只要『劍騎士』和『黑之聖騎士』被引到國境附近，在這期間就能展開蹂躪了。人的死亡與絕望可是魔神大人的精神食糧呀。」
『不過那裡不是還有『冰麗的魔人』和『鐵人』在嗎？』
「哼！個人的力量再這麼優秀，力量也是有限的，靠品質贏得數量是做不到的。」
『嗯，怎麼樣都行，只要這次計畫成功，就離魔神大人的復活進了一步。』
「失敗是不可能的吧！根據你的調查，以士兵最少的時機進攻這條策略確實成功了。」
『是啊......確實沒有失敗的理由。』

從水晶中傳來了男人認同的聲音。

『「哈哈哈哈哈！一切，都是為了魔神大人！」』

男人們說出了對組織絕對忠誠的話語。
但是，現在男人們還不知道。
在他們即將攻打的地方，存在著超越魔神的『人類』（怪物）的事實。

