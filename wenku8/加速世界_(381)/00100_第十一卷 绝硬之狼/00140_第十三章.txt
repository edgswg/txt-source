春雪持續抵抗睡意，撐完下午的兩堂課與簡短的導師時間。從樓梯口一走到外面，雨點正好打在鼻頭上。

朝天空一看，深灰色烏雲以媲美「轟雷」空間的密度層層堆積。虛擬桌面上的天氣預報顯示，從下午三點半起會降下每小時二點五公厘的雨量。這樣的雨量會讓運動性社團停止戶外練習，但飼育委員的工作當然不受影響。

春雪小跑步到後院，先跟小咕打招呼，接著以相當於平常一點二倍的速度打掃小木屋。也不知道該不該說遺憾，同僚井關寄來了一封五彩繽紛的純文宇郵件，內容是說她今天要忙著準備校慶，沒辦法整理小木屋，絕對不是因為下雨才偷懶。

洗完了給小咕洗澡用的水盆，春雪背後便傳來一陣輕快的腳步聲。回頭一看，一名打著紅傘小跑步跑來的少女映入眼簾。她正是飼育委員會超委員長四堃宮謠，不過裝扮似乎和平常有些不太一樣。仔細一看，原來她的腳下也穿著火紅的長靴。

「午、午安，四堃宮學妹。」

春雪抱著水盆打招呼之餘，視線不由得盯著這雙防水纖維的長靴不放。他心想，以前自己下雨時也穿過這樣的鞋子，到底什麼時候開始不穿的呢？就在此時——

【UI〉有田學長午安。學長這樣一直看，我會有點不好意思。】

半透明的聊天視窗上浮現出這串文字，視窗後則可以看到兩只長靴忸忸怩怩地動著。

「啊……對、對對、對不起！」

春雪有點慌，心想再這樣下去「戀足癖之男」這個外號真的會跟定他，於是大喊：

「我、我是覺得、這雙長靴很可愛！」

下著雨的後院突然鴉雀無聲。謠滿臉通紅地低頭，春雪則是話出了口才搞清楚自己說了什麼，不由得陷入當機狀態。最後救了他們的，是小咕拍響翅膀表示肚子餓的抗議聲。

角鴞從謠的手上吃完一整盒肉片，秀了一段餐後運動的回旋飛行才回到栖木上。

春雪抬頭看著立刻轉移到瞌睡模式的小咕，輕聲說：

「它對這間小木屋好像也習慣多了。」

謠脫卜左手的保護手套，點點頭閃動手指：

【UI〉嗯，我也沒想到它才一周就能過得這麼安穩。多虧了各位飼育委員的努力。】

「哪裡……我只有在掃地……而且小咕好像也比較喜歡井關同學……」

或許是這句話聽來有點像鬧別扭，謠嘻嘻一笑，打著字說：

【UI〉沒的事。小咕它相當信任有田學長呢。冉過一陣子，我想請學長也幫忙餵食。】

「咦？可是，不是說小咕只會吃你手上的東西……」

他反射性說到這裡就閉上嘴，一秒鐘後，改變語調問：

「四堃宮學妹，你這麼做該不會是……因為小咕受到上一個飼主的傷害……？」

聽到這句話，謠停下收拾工具的手，直視春雪。她眨了眨一對大眼睛，同時點點頭回答：

【UI〉仔細看看小咕的左腳，就能看見取出微型晶片的傷痕還留著。】

春雪看完這行字，震驚地抬起視線，注視已經收起耳羽，閉著雙眼打瞌睡的角鴞左腳。上面的確有著一道長約兩公分，疑似被刀刃縱向割開的疤痕。

「……好過分……那麼大一道傷口……」

春雪咬緊嘴唇，握緊雙拳。

白臉角鴞屬於猛禽類，個人飼養起來多半會很辛苦。畢竟它能吃的食物比較特殊，還需要相當大的鳥籠。但在寵物店購買的時候，店員應該已經說明過這些事項。即使後來有不得已的苦衷，也萬萬不該為了省錢就拿刀刃挖出微型晶片，把受傷的寵物就這麼遺棄在外。

小咕得以保住性命，像現在這樣快活，乃是萬中無一的奇跡。春雪再次認知到這點，喃喃說道：

「一定是四堃宮學妹拚命照料，它才能得救啊……」

這句話說完，隔了一會兒後，才有一串櫻花色的文字略顯遲疑地顯示出來。

【UI〉我……絕對不想再看到有生命從我手中消逝。】

春雪花了幾秒鐘理解這一行字意味著什麼，登時摒息。

這也就是說——之前曾經有生命從謠的手中消逝。而且肯定不是指像小咕這樣的寵物，而是人……是謠的親生哥哥，同時也是讓她成為超頻連線者的「上輩」四堃宮竟也。

根據謠昨天所說，竟也是在能舞台的「鏡房」發生被巨大三面鏡壓倒的意外而喪命。謠說自己也在場，但相信事情不是這麼簡單。看到鏡子碎片造成的出血，年幼的謠多半伸手想去止血。但她依舊徒勞無功，竟也還是回不來了。

春雪在腦海中描繪出這悲傷的情景，突然注意到一件事，瞪大了眼睛。

……四堃宮謠的對戰虛擬角色「劫火巫女」Ardor　Maiden，她那上半身雪白，下半身緋紅的模樣……那種清澈卻又沉重的深紅色，該不會就是……

春雪從小咕身上移開視線，看著站在身旁的謠。看著她身穿松乃木學園純白的制服與紅色長靴的模樣。

謠似乎從春雪的眼神看穿了其心思，只見她微微一笑，點點頭說：

【UI〉從那一天起，我虛擬角色的紅褲色彩就慢慢改變。從淡淡的粉紅……轉變成深沉的緋紅。上不定，那就是竟也哥哥鮮血的顏色。】

之後好一陣子，兩人默默做著手上的工作——打掃小木屋、倒垃圾、提報日志檔案。

所有任務完成後，春雪仍然遲遲不敢開口。

所有對戰虛擬角色都會有象徵個性的顏色，造型上有著兩種色調的例子也不少。像春雪的Silver　Crow，就可以分為銀色的金屬裝甲部分與深灰色的身體部分。

所以，即使有個上半身白色，下半身粉紅色的虛擬角色存在，這種程度的色彩差異也不會讓人覺得不可思議。就色相環來說，應該可以歸類在「略偏遠程攻擊的白色系」。

Ardor　Maiden之所以顯得特異，是因為有著白與緋紅這兩種相距甚遠的顏色。昨天謠說會有這種顏色的理由，是因為她兼有「本來的自己」與「身為能樂子方的自己」這兩面，但事實絕非這麼簡單。從謠以幼小的手抱住身受重傷的哥哥，拚命想幫他止血的那一天起，謠的下半身就染上了深紅色。

所以對戰虛擬角色的紅褲也變了顏色——而謠多半也就是因為這件事失聲……

「…………四堃宮學妹，對不起。」

聽到春雪突然道歉，在板凳前正要背起書笆的謠轉過身來，微微歪了歪頭。

「昨天你跟我說了那麼多……全都是為了讓我學會『理論鏡面』能力……可是，我從昨天到現在，滿腦子都是另一件事……」

要是春雪昨天傍晚離開謠家以後立刻回家，沒有興起跑去中野區對戰的念頭，就不會遭遇到Wolfram　Cerberus，也不會被打得一敗塗地，今天也就可以專心想「鏡子」的事了。謠真摯地告訴春雪親生哥哥之死這種再悲傷不過的事，光是為了報答這份心意，他就應該分秒必爭地趕快學會鏡面能力。然而春雪從昨晚敗戰之後，滿腦子都沒辦法去想其他事。

「…………真的，很對不起。可是……可是我…………」

春雪再也說不下去，深灤垂下頭。

於是謠先穩穩地背起書包，接著以紅色長靴踩過積水直線朝他走去。最後少女在他眼前停步，微笑著打字表示：

【UI〉學長不需要道歉。因為……學長你知道嗎？我從剛剛就期待得不得了呢。】

「咦……期待？期待什麼……？」

【UI〉當然……就是期待坐在特等席，看鴉鴉找那個Wolfram　Cerberus報仇雪恨羅。】

「……什、什麼？」

【UI〉時間也差不多了，我們就馬上過去吧。】

接著，她就在什麼話也說不出來的春雪面前撐開紅傘。春雪也半自動地從板凳上提起自己的書包，拿出一把折疊傘。撐開傘的輕響才剛傳出來，雨勢就彷佛收到信號似的大大增強。

「呃，這個……Cerberus的事，是黑雪公主學姊跟你說的……？」

為了不被雨聲蓋過，春雪微微加大音量這麼一問，謠就理所當然地點點頭回答：

【UI〉是的。幸幸說，要我代替她和楓姊好好見證鴉鴉的對戰。】

「是、是這樣啊……」

——這下子，今天無論如何都得好好打，不然明天的特訓份量就會增加到兩倍……不，是三倍。

儘管內心因此戰慄不安，至今仍未消失的遲疑卻仍然讓春雪腳步沉重。謠從傘緣下抬頭看著遲遲不邁出腳步的他，右手手指跳動打字：

【UI〉有田學長，我是這麼想的……昨天鴉鴉會遇到Wolfram　Cerberus，是冥冥中自有安排。】

「安排……？」

【UI〉是。「理論鏡面」對於光屬性攻擊有著絕對的抗性，「物理無效」則可以完全彈開物理屬性攻擊。這兩種能力處在兩個極端，但也因此非常接近……至少我是這麼覺得。那麼，跟Cerberus對戰，肯定是鴉鴉到達「鏡子」境界不可或缺的過程。】

「是……這樣……嗎？」

就在春雪喃喃說出這句話的時候，原以為吃飽了正午睡的小咕，卻在小木屋內大動作拍響翅膀，還「咕咿！」地叫了一聲。謠立刻打字說：

【UI〉你看，小咕也叫你加油。】

這讓春雪只能苦笑。他先看看小木屋內的角鴞，再看看紅傘下的謠，點點頭說：

「……嗯。要是現在不去，總覺得會變成是拿『理論鏡面』當藉口，逃避跟他再戰。而且學姊也說，所有努力都會匯集在同一點。」

【UI〉就是這樣！】

謠用力敲下虛擬的Enter鍵，以同一只手用力握了握春雪左手腕，這才轉身用穿著長靴的腳跑向雨中。

從後院抵達前庭，接著走出校門左轉走一小段路，就能來到寬廣的青梅大道。

昨天春雪是從比這裡偏南許多的方南大道進入中野區，但現在春雪他們所在的杉並第二戰區卻與中野第二戰區相鄰。而且相鄰的界線往南北向延伸得很長，所以只要往東走，怎麼走都到得了。

春雪與謠並肩沿青梅大道往東走，同時打開虛擬桌面上的導航地圖。他調整地圖比例尺，按出昨天跟Cerberus對打時所在的中野車站周邊地圖，之後半自言自語地說：

「繼續在這條路上走個一點五公里，也可以到中2區……可是既然要去中野車站，從高圓寺搭電車應該比較好吧？不過這樣就跟四堃宮學妹的家反方向了……」

背後傳來一個平穩的聲音說：

「與其搭電車，還不如搭青梅大道往都心方向的公車來得快吧。而且，再三分鐘正好有一班開往中野車站的公車會到站。」

「啊，對喔，這條路上當然也有公車了。平常沒在用，我都忘了有這回事……」

春雪看著地圖搔搔頭，這次則傳來一個拿他沒輒的聲音。

「我說你喔，每天放學回家路上明明都會看到那麼多輛公車耶。小春你從以前就是這樣，對沒興趣的東西都視若無睹。」

「才、才沒有。班上同學的長相，我就記住了八成左右………………等等。」

這時春雪才注意到自己正在跟別人直接對話，驚訝得跳了起來。他以右手握住的雨傘為軸心，旋轉一百八十度，交互看了看兩張再熟悉不過的臉孔。

「咦……阿、阿拓小百？你們為什麼在這裡！」

「小春我說你喔，我是不會不准把我們兩個連在一起叫啦，可是至少該女士優先吧？」

「那、那小百阿拓……可是這樣叫，聽起來又好像在吉野家點餐。（注：諧音冷笑話，原文為チユタク和ツユタク）」

春雪說完，不由得想像起湯汁加量的牛丼，趕緊搖搖頭告訴自己不是在說這個。

「我、我是要問，你們怎麼會在這裡？」

他這麼一問，把竹刀袋靠在左盾上、右手還撐著藍色雨傘的修長男生——黛拓武，理所當然地回答：

「雨下這麼大，我跟小千的社團都提早結束了活動。所以我們才在校門口等，想去幫你加油啊。」

接著，身旁斜背著一個大型運動提袋的短髮女生——倉嶋千百合滿臉甜笑地開口：

「我跟小拓等的時候就在打賭，小春滿腦子都只想著對戰，究竟會不會注意到我們呢？結果是徹底沒看到！看，我就說你都視若無睹吧！」

「嗚……順、順便問一下，誰贏了？」

「那還用說？當然是我跟小拓贏，小春輸！今天回家路上，你要請我們各一杯珍珠豆漿香蕉歐蕾！」

「等……哪、哪有這樣擅自決定的啦……」

千百合對愣住的春雪投以火力稍微調低的視線光束。

「兩個超級好朋友下這麼大雨還願意幫你加油，你卻完全沒看到，請這點東西也只是剛好而已吧！」

春雪一時啞口無言。旁邊始終笑嘻嘻聽著他們談話的謠也補上最後一刀：

【UI〉我當然早就注意到他們兩位了。】

「…………對、對不起……」

春雪搓著雙手食指道歉，拓武則一如往常地抓準時機，對他伸出援手：

「你們看，公車來了。」

「喔，真的耶！快跑快跑！」

春雪立刻跑向前方的公車站牌，背後的千百合則大喊：「你別想跑！」

四人魚貫搭上EV公車，所幸最後排的座位都是空的。春雪、謠、千百合、拓武從右到左依序坐好，不約而同地鬆了一口氣。比起待在不舒服指數全滿的六月雨中，空調夠強的車內簡直就是天堂。

「這麼說來，我今天要去中野這件事，小百你們也是聽學姊她們說的嗎？」

春雪隔著走道朝坐在謠對面的千百合這麼一問，這位兒時玩伴便輕輕搖了搖頭：

「沒有，我是聽小拓說的。至於小拓他呢……」

「應該算是……從知識和經驗做出推測吧。以昨天小春的沮喪度來看，我想大概今天之內就會振作起來，跑去打雪恥戰。」

另一個兒時玩伴用手指頭把眼鏡往上一推，說出這樣的話來。

【UI〉三位的默契真不是蓋的。】

謠以佩服的表情打出這行字，千百合卻當場丟出「只是小春太好懂而已啦！」這種不留情面的評語。

聊著聊著，公車很快就開過了高圓寺陸橋路口——春雪每兩天就會跟Ash　Roller交手對戰的地點——接近杉並與中野的界線，離進入新區域只差兩個紅綠燈。千百合表情轉為嚴肅，小聲對春雪問說：

「要怎麼辦？在中野車站附近找能坐的地方，還是說……」

「反正外面下雨，直接在公車上就好了。一進入中野我就馬上開始。」

聽春雪這麼說，三人同時點了點頭。接著四人一起把身體靠在座位的椅背上，做好加速的準備。公車在下著雨的青梅大道上行駛得十分順暢。當通過第一個綠燈、接近第二個綠燈時，春雪深深吸一口氣。

都來到這裡了，再掙扎也沒有意義。能做的事都去做，這樣就對了。

公車越過了以AR方式顯示在視野中的紅色界線。

等了一秒鐘後，春雪在最小的音量中灌注所有的鬥志，喊出了指令：

「————超頻連線！」
