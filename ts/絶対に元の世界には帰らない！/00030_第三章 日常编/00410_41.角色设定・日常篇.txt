佐藤陰矢 男性 異世界人 ２２歳
總感覺像是戀愛喜劇裡主角的主角。
雖然膽小怕事又被動的小市民性格還是沒變，但似乎是無意識播光彌給感化了，變得會幫助別人了。
戀愛Falg的插旗能力也順便學到了，但因為經驗值為零完全沒法好好使用。
用動物來比喻的話就是貓。從外表看就是個不知道在想什麼的生氣臉。但一旦熟稔後就時不時會撒嬌了。
雖然不能說是什麼，但肯定很弱。

佐藤影耶 女性 異世界人 １７歳
又是主角又是女主角的女主角。⋯⋯恩，就是女主角呢。
雖然曾經談論過對胸部的堅持，但其實最堅持的好像是腰部的平衡。突入日常篇後展現了女僕、泳裝、偽巫女服、蘿莉等眾多新的一面。
因為這個狀態下是演技模式，所以會毫不猶豫地說出一些有點羞人的台詞。
用動物來比喻的話就是貓。雖然很可愛卻很難搞，一不注意就不知道跑哪裡去了。但一旦熟稔後就時不時會撒嬌了。
雖然不能說是什麼，但肯定很弱。

鈴木光彌 男性 異世界人 １６歳
主角外掛全套啟動的主角。⋯⋯恩，就是主角。
透過辛苦鍛鍊獲得可以壓倒的影耶的力量。啊，姑且說下被隕石砸中還是會死的。
經過和伊亞以及魔王等過去的自己絶對無法獲勝的強敵的戰鬥，精神方面也成長了許多。
還有就是這雖然是裏設定，但光彌老家是有錢到沒天理的地步，就算帶影耶和艾伊西亞回去也能簡單養活。

艾伊西亞 女性 人類 １５歳
意外好攻略的王女大人。有憧憬英雄的傾向。
雖然還沒徹底放棄光彌，但也喜歡上了影耶。現在正一邊準備魔王的對策，一邊改良異世界移動的魔法陣。
啊，還有就是胸部意外的大。因為還在成長途中，只要再加油一點就能超越影耶了。

安古 男性 人類 ３０歳
雖然活躍都不顯眼，但還是默默地努力著的大叔。
目前作為冒険者的對魔王對策組織的領導努力奮鬥中。

夏緹亞 女性型 魔人 ?歳
沈迷來自異世界的文明利器，徹底化為廢人的魔人美女。順帶一提，魔人因為是只有一代的存在所以本來並沒有性別。在這之上還注明是「女性型」也是因為這樣。雖然可以產下小孩，但生下的小孩一定會是對方的種族。
現在正在思考和魔王對戰時的決勝台詞。

莉賽布 女性 人類 ２２歳
公會的接待小姐。最近因為麻煩的工作增加太多而累得半死。在想說要不要去聯誼，還想要邀請影耶一起去。今天的她也在認真工作。

伊緹 女性 人類 ２７歳
王家的女僕。沒有對任何人說出陰矢和影耶的秘密。雖然FLAG立了起來，但真的有回收FLAG的一天嗎。

羅吉
旅店主人。日常篇幾乎沒有戲份。

隆
旅店兒子。稍微出場了一下。

齊沙
咖啡店的店主所以是齊沙。還是一樣簡單明了的命名。現在也時不時會做隱藏菜單的料理。（興國：咖啡店（喫茶店）音近齊沙的日文發音キーサ）

紗祺
把齊沙倒過來念就是紗祺。還是一樣簡單明了的命名。店面順利地繁盛起來了。

菲露斯
刻耳柏洛斯＋芬里爾→刻耳菲露斯→菲露斯。穿著有雙頭裝飾的冰鎧的魔狼。等級來說差不多是四天王那邊。
雖然夏緹亞是主人，但也沒太聽她的話。

破滅將普拉特
淫魔的最高階種，影之四天王。角色出生理由是「啊該死，本來打算留下一個四天王的竟然忘記了。」

比起直接戰鬥更重視對軍戰鬥的能力構成。可以連射大規模魔法，就算沒有魔王核也有著僅次魔王的殲滅力。

龍王斯佩庫魯
老爺爺龍族。只要稍微撒嬌下素材就會不停落下。
雖然在迷宮中可以封殺魔王，但直接戰鬥力相當低。並不是本人沒有力量，而是巨大的力量幾乎都花費在迷宮的維持上。嘛說低也比那邊的頭目魔物還強。

魔王薩尼烏斯
魔王。快要復活了。
雖然力量有著驚人的進步⋯⋯但最後究竟會怎樣呢。


********************

有名字的角色意外的少⋯⋯⋯之後可會追加什麼。